﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Mapping
{
    internal class MapperConverter
    {
        /// <summary>
        /// The type to convert before serialization.
        /// </summary>
        internal readonly Type Type;
        /// <summary>
        /// The type to convert to. This one is used for serialization of further conversion if specified,
        /// </summary>
        internal readonly Type TypeIntermediate;
        /// <summary>
        /// The type that is actually serialized (the type at the end of the chain of converters).
        /// </summary>
        internal Type TypeFinal => Chained != null ? Chained.TypeFinal : TypeIntermediate;
        private readonly Func<object, object> Serializer;
        private readonly Func<object, object> Deserializer;

        public MapperConverter Chained;

        internal MapperConverter(Type type, Type typeIntermediate, Func<object, object> serializer, Func<object, object> deserializer)
        {
            Type = type;
            TypeIntermediate = typeIntermediate;
            Serializer = serializer;
            Deserializer = deserializer;
        }

        //internal object Serialize(object @object) => Serializer(@object);
        //internal object Deserialize(object @object) => Deserializer(@object);

        internal object Serialize(object @object) => Chained != null ? Chained.Serialize(Serializer(@object)) : Serializer(@object);
        internal object Deserialize(object @string) => Chained != null ? Deserializer(Chained.Deserialize(@string)) : Deserialize(@string);

        internal void Chain(IEnumerable<MapperConverter> converters)
        {
            Chained = null;

            foreach (MapperConverter converter in converters)
            {
                if (TypeIntermediate == converter.Type)
                {
                    Chained = converter;
                }
            }
        }
    }
}
