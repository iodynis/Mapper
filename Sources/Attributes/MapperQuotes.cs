﻿using System;

namespace Iodynis.Libraries.Mapping
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MapperQuotes : Attribute
    {
        public bool UseQuotes { get; }
        public MapperQuotes(bool useQuotes)
        {
            UseQuotes = useQuotes;
        }
    }
}