﻿using System;

namespace Iodynis.Libraries.Mapping
{
    /// <summary>
    /// Comment to write along with the mapped property value.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class MapperComment : Attribute
    {
        /// <summary>
        /// The comment.
        /// </summary>
		public string Comment { get; }
        /// <summary>
        /// Comment to write along with the mapped property value.
        /// </summary>
		public MapperComment(string comment = null)
		{
			Comment = comment;
		}
    }
}
