﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace Iodynis.Libraries.Mapping.Example
{
    public class TestObject : INotifyPropertyChanging, INotifyPropertyChanged
    {
        public event PropertyChangingEventHandler PropertyChanging;
        private void NotifyPropertyChanging([CallerMemberName] string propertyName = null)
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private bool _Boolean;
        public event EventHandler<bool> BooleanChanged;
        [MapperSection("Simple")]
        public bool Boolean
        {
            get
            {
                return _Boolean;
            }
            set
            {
                if (_Boolean == value)
                {
                    return;
                }
                NotifyPropertyChanging();
                var valueOld = _Boolean;
                _Boolean = value;
                BooleanChanged?.Invoke(this, value);
                NotifyPropertyChanged();
            }
        }
        private int _Integer;
        public event EventHandler<int> IntegerChanged;
        [MapperSection("Simple")]
        public int Integer
        {
            get
            {
                return _Integer;
            }
            set
            {
                if (_Integer == value)
                {
                    return;
                }
                NotifyPropertyChanging();
                var valueOld = _Integer;
                _Integer = value;
                IntegerChanged?.Invoke(this, value);
                NotifyPropertyChanged();
            }
        }
        private float _Float;
        public event EventHandler<float> FloatChanged;
        [MapperSection("Simple")]
        public float Float
        {
            get
            {
                return _Float;
            }
            set
            {
                if (_Float == value)
                {
                    return;
                }
                NotifyPropertyChanging();
                var valueOld = _Float;
                _Float = value;
                FloatChanged?.Invoke(this, value);
                NotifyPropertyChanged();
            }
        }
        private double _Double;
        public event EventHandler<double> DoubleChanged;
        [MapperSection("Simple")]
        public double Double
        {
            get
            {
                return _Double;
            }
            set
            {
                if (_Double == value)
                {
                    return;
                }
                NotifyPropertyChanging();
                var valueOld = _Double;
                _Double = value;
                DoubleChanged?.Invoke(this, value);
                NotifyPropertyChanged();
            }
        }
        private string _String;
        public event EventHandler<string> StringChanged;
        [MapperSection("Simple")]
        public string String
        {
            get
            {
                return _String;
            }
            set
            {
                if (_String == value)
                {
                    return;
                }
                NotifyPropertyChanging();
                var valueOld = _String;
                _String = value;
                StringChanged?.Invoke(this, value);
                NotifyPropertyChanged();
            }
        }
        private Color _Color;
        public event EventHandler<Color> ColorChanged;
        [MapperSection("Complex")]
        public Color Color
        {
            get
            {
                return _Color;
            }
            set
            {
                if (_Color == value)
                {
                    return;
                }
                NotifyPropertyChanging();
                var valueOld = _Color;
                _Color = value;
                ColorChanged?.Invoke(this, value);
                NotifyPropertyChanged();
            }
        }
        private Size _Size;
        public event EventHandler<Size> SizeChanged;
        [MapperSection("Complex")]
        public Size Size
        {
            get
            {
                return _Size;
            }
            set
            {
                if (_Size == value)
                {
                    return;
                }
                NotifyPropertyChanging();
                var valueOld = _Size;
                _Size = value;
                SizeChanged?.Invoke(this, value);
                NotifyPropertyChanged();
            }
        }

        public TestObject()
        {
            ;
        }
    }
}
